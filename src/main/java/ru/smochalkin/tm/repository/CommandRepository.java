package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.ICommandRepository;
import ru.smochalkin.tm.constant.ArgumentConst;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display version."
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of terminal commands."
    );
    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display list of arguments."
    );
    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display list of commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close the program."
    );

    public static final Command[] ALL_COMMANDS = new Command[] {
            ABOUT, VERSION, INFO, HELP, ARGUMENTS, COMMANDS, EXIT
    };

    public Command[] getCommands(){
        return ALL_COMMANDS;
    }

}
